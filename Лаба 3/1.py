import pygame
from pygame.draw import *

pygame.init()

FPS = 30
screen = pygame.display.set_mode((400, 400))

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)

screen.fill(WHITE)
circle(screen, YELLOW, (200, 200), 100)

circle(screen, RED, (250, 170), 25)
circle(screen, RED, (150, 170), 20)
circle(screen, BLACK, (250, 170), 10)
circle(screen, BLACK, (150, 170), 10)

polygon(screen, BLACK, ([150, 250], [250, 250], [250, 260], [150, 260]))
polygon(screen, BLACK, ([220, 170], [330, 125], [300, 120], [230, 150]))
polygon(screen, BLACK, ([180, 175], [70, 125], [90, 120], [180, 150]))



pygame.display.update()
clock = pygame.time.Clock()
finished = False

while not finished:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True

pygame.quit()
import turtle
import math

t = turtle
t.speed(1)
turtle.shape('turtle')

def F(n, a):
    u =  ((n - 2) * 180) / n
    t.left((180 - u) / 2)
    for i in range(n):
        t.pendown()
        t.forward(a)
        t.left(180 - u)
    t.right((180 - u) / 2)

r = 20

t.left(90)
t.dot(4)
for n in range(3, 13):
    a = abs(r * (2 * math.sin(math.radians(180 / n))))
    t.penup()
    t.goto(r, 0)
    F(n, a)
    t.penup()
    r += 10

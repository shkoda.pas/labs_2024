import turtle
import math

t = turtle
t.speed(100)
turtle.shape('turtle')

def cirL(k, n):
    for j in range(n):
        t.pendown()
        for i in range(36):
            t.forward(k)
            t.left(5)
        t.penup()
    
def cirR(k, n):
    for j in range(n):
        t.pendown()
        for i in range(36):
            t.forward(k)
            t.right(5)
        t.penup()


t.left(90)
t.penup()
t.goto(80, 0)
t.color("yellow")
t.begin_fill()
cirL(7, 2)
t.end_fill()

t.goto(-15, 40)
t.color("blue")
t.begin_fill()
cirL(1, 2)
t.end_fill()

t.goto(15, 40)
t.color("blue")
t.begin_fill()
cirR(1, 2)
t.end_fill()

t.goto(0, 20)
t.left(180)
t.width(4)
t.color("black")
t.pendown()
t.forward(20)
t.penup()

t.goto(30, -10)
t.color("red")
cirR(3, 1)

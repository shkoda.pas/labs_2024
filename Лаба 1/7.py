import turtle
t = turtle
turtle.shape('turtle')
t.pendown()
k = 1
r = 5
for i in range(500):
    t.forward(i / 100 * 3.14)
    t.left(r)

import turtle
import math

t = turtle
t.speed(1)
turtle.shape('turtle')

def F(n):
    u =  ((n - 2) * 180) / n
    t.pendown()
    for i in range(n):
        t.forward(100)
        t.left(180 - (180 - u) / 2)
    t.penup()


r = 20

t.left(90)
t.penup()
t.goto(100, 0)
F(5)
t.goto(-100, 0)
F(11)

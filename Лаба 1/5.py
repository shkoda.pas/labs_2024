import turtle

t = turtle
turtle.shape('turtle')
k = 1

for i in range(10):
    for j in range(4):
        t.pendown()
        t.forward(20 * k)
        t.right(90)  
    t.penup()
    t.goto(-10 * k, 10 * k)
    k += 1

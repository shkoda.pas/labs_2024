import turtle

t = turtle
turtle.shape('turtle')
k = 1
n = int(input())
r = 360 / n
for i in range(n):
    t.pendown()
    t.forward(50)
    t.right(180)
    t.penup()
    t.forward(50)
    t.right(180 + r)

import turtle
import math

t = turtle
t.speed(100)
turtle.shape('turtle')

def Fr(r, a):
        for i in range(360 // r):
            t.forward(a)
            t.right(r)
        for i in range(360 // r):
            t.forward(a)
            t.left(r)

r = 10
a = 5
t.left(90)
for i in range(6):
    Fr(r, a)
    a += 2

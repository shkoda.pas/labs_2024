import turtle
import math

t = turtle
t.speed(100)
turtle.shape('turtle')

def Fr(k):
    if (k % 2 == 0):
        for i in range(36):
            t.forward(5)
            t.right(5)
    else:
        for i in range(18):
            t.forward(2)
            t.right(10)


t.left(90)
for k in range(7):
    Fr(k)

import pygame
from pygame.draw import *
from random import randint


# создание картежа с характерисиками шарика и его отображение
def new_ball(color):
    x = randint(100, 700)
    y = randint(100, 500)
    r = randint(30, 50)
    circle(screen, color, (x, y), r)
    xpos = randint(0, 1)
    ypos = randint(0, 1)
    ball = [x, y, r, xpos, ypos, color]
    return ball


# смещение и отражение шарика
def ball_move(balls):
    for ball in balls:
        if ball[0] + ball[2] >= 1200:
            ball[3] = 0
        elif ball[0] + ball[2] <= 2 * ball[2]:
            ball[3] = 1

        if ball[3]:
            ball[0] += 10
        else:
            ball[0] -= 10

        if ball[1] + ball[2] >= 900:
            ball[4] = 0
        elif ball[1] + ball[2] <= 2 * ball[2]:
            ball[4] = 1

        if ball[4]:
            ball[1] += 10
        else:
            ball[1] -= 10
        circle(screen, ball[5], (ball[0], ball[1]), ball[2])
    return balls


# установка констант цветов
RED = (255, 0, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
GREEN = (0, 255, 0)
MAGENTA = (255, 0, 255)
CYAN = (0, 255, 255)
BLACK = (0, 0, 0)
COLORS = [RED, BLUE, YELLOW, GREEN, MAGENTA, CYAN]
# счетчик попаданий по шарикам
count = 0

pygame.init()
# частота обновления дисплея
FPS = 30
# размеры дисплея
screen = pygame.display.set_mode((1200, 900))

pygame.display.update()
clock = pygame.time.Clock()
finished = False

# количество шариков
N = 5
# массив шариков
balls = [new_ball(COLORS[randint(0, 5)]) for x in range(N)]
while not finished:
    now = 0
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True
        # проверка нажатия на каждый шарик
        elif event.type == pygame.MOUSEBUTTONDOWN:
            for ball in balls:
                if (ball[0] - event.pos[0]) ** 2 + (ball[1] - event.pos[1]) ** 2 <= ball[2] ** 2:
                    count += 1
                    # пересоздание шарика по которому попали
                    balls.pop(now)
                    balls.append(new_ball(COLORS[randint(0, 5)]))
                now += 1
    # смещение всех шариков
    balls = ball_move(balls)
    # обоновление дасплея
    pygame.display.update()
    # заливка дисплея ченым цветом
    screen.fill(BLACK)

print('Final score - ', count)
pygame.quit()

import turtle
from random import *

t = turtle
for i in range(100):
    if (random() % 2 == 0):
        t.left(randint(1, 360))
        t.forward(randint(1, 100))
    else:
        t.right(randint(1, 360))
        t.forward(randint(1, 100))

import turtle
output = open('3.txt', 'w')
print(20, -40, 270, 40, 225, 20, 225,'\n',25, -40, 0, 20, 270, 20, 270, 20, 90, 20, 90, 20,'\n',50, -40, 270, 20, 45, 28, 225, 20, 180, 20,'\n',75, -40, 0, 20, 270, 20, 270, 20, 90, 20, 90, 20,'\n',100, - 40, 270, 40, 90, 20, 90, 40, 90, 20, 180,'\n',125, -40, 315, 28, 315, 20, 270, 20, 270, 20, 270, 20, file=output)
output.close()

t = turtle
f = open('3.txt', 'r')
x = f.readline()
while x != '':
    x = list(map(int, x.split()))
    for i in range(1, len(x)):
        if i == 1:
            t.penup()
            t.goto(x[0], x[1])
            t.pendown()
        else:
            if i % 2 == 0:
                t.right(x[i])
            else:
                t.forward(x[i])
    x = f.readline()
f.close()
#with open('3.txt') as f:
  #  for x in f:
    #    c = list(map(int, x.split(', ')))

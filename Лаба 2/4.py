import turtle
T = turtle

Vx = 5
Vy = 40
g = -10
x = -200
y = 0
t = 0
T.penup()
T.goto(-200, 0)
T.pendown()
for i in range(10):
    while True:
        x += Vx * t
        y += Vy * t + g*t ** 2 / 2
        Vy += g * t
        T.goto(x, y)
        t += 10 ** (-3)
        if y < 0:
            break
    Vy = abs(Vy) / 1.3
